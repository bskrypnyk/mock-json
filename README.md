Mock JSON library for Node.js based on mockJSON jquery plugin
=============================================================

Generates random json mock data based on templates:

	* @NUMBER
	* @LETTER_UPPER
	* @LETTER_LOWER
	* @MALE_FIRST_NAME
	* @FEMALE_FIRST_NAME
	* @FIRST_NAME
	* @LAST_NAME
	* @EMAIL
	* @DATE_TIME
	* @DATE_YYYY
	* @DATE_DD
	* @DATE_MM
	* @TIME_HH
	* @TIME_MM
	* @TIME_SS
	* @LOREM
	* @LOREM_IPSUM

In addition, modifiers are supported for numeric values:

	* |N-M : length range from N to M
	* |+1  : incremement value

For example:

	var mock = require( './mock-json.js' );
	
	var tmpl1 = {
		
		"entries|1-10" : [
			{
				"id|+1"					: 0,
				"name"					: "@FIRST_NAME @LAST_NAME",
				"eg_date_time"			: "@DATE_TIME",
				"eg_date_assembled"		: "@TIME_HH:@TIME_MM:@TIME_SS",
				"title|2-4"				: "@LOREM ",
	            "description"			: "@LOREM_IPSUM",
	            "age|18-31"				: 18, 
	            "married|0-1"			: true,
	            "subentries|0-3"		: [
	            	{
		            	"comment|1-10"		: "@LOREM_IPSUM",
	            	}
	            ]
			}
		]
	};
	var r = mock.generate( tmpl1 );

Will result in:

	{ 
     "entries" : [ 
       { 
         "id" : 0,
         "name" : "Margaret Harris",
         "eg_date_time" : "Sat May 06 2006 20:54:21 GMT-0700 (PDT)",
         "eg_date_assembled" : "08:31:52",
         "title" : "eiusmod qui dolore dolor ",
         "description" : "proident, consequat. amet, ut cillum aute velit nisi eiusmod aliqua. Ut nulla",
         "age" : 29,
         "married" : false,
         "subentries" : [ 
           { 
             "comment" : "ex in voluptate commodo culpa dolore aliqua. aliqua. tempor eiusmod ad dolor aute ipsum et sint ad ullamco elit, proident, amet, dolor et aliqua. quisunt adipisicing ut occaecat ex ad est Duis sint adipisicing esse irure et minim aliquip ullamcoexercitation officia et reprehenderit exercitation incididunt do etdolor nostrud fugiataliqua. labore Excepteur est adipisicing consectetur ipsum cillum exercitation aute dolore proident, aliqua. pariatur. minim et quis deserunt tempor cupidatat elit, est cillum Lorem magna officia aliqua. aliqua. velit"
             },
           { 
             "comment" : "adipisicing sit sit laborum quis elit, ea commodo ad Lorem ad Duis reprehenderit eu ipsum Lorem proident, ipsum veniam, dolor do ullamcoeiusmod in irure sed dolore commodo proident, elit, sunt exercitation in non dolore commodo non aliquip ex aliquip consectetur voluptate nulla eanulla pariatur. nisi occaecat Excepteur irure in et nostrud occaecat pariatur. aliqua. officia amet, dolor non laborum Lorem in culpa in"
             },
           { 
             "comment" : "laborum amet, veniam, in ullamco nostrud nisi deserunt enim minim incididunt officia non nostrud dolore non dolore sint nulla esse dolor dolore dolor idExcepteur officia amet, esse proident, sunt occaecat Ut esse Lorem ut veniam, Loremnisi commodo in incididunt dolore ut Duis ex sed adipisicing ipsum quiincididunt Excepteur aliqua. minim ad qui est veniam, sint eu deserunt autetempor Duis nisi Duis reprehenderit pariatur. ipsum et eiusmod ad mollit sed velitnostrud laboris nisi Ut ullamco do minim ex sed reprehenderit reprehenderit dolor tempor ipsumconsectetur do ex et exercitation reprehenderit occaecat sunt qui ea nisi consectetur quis aliquip dooccaecat exercitation cillum laborum ut eu anim suntex minim ullamco elit, sed Excepteur ad aute culpa ea ullamco elit, ullamco sunt proident, ullamco irure"
             }
           ]
         },
       { 
         "id" : 1,
         "name" : "Linda Johnson",
         "eg_date_time" : "Sat Mar 03 1973 13:05:27 GMT-0800 (PST)",
         "eg_date_assembled" : "08:32:13",
         "title" : "incididunt anim exercitation ",
         "description" : "laboris magna occaecat sed ullamco aute nulla anim do dolore tempor Excepteur tempor consequat. id cupidatat deserunt eiusmod adipisicing dolor proident, fugiat in pariatur. aliqua. cillum ea sint adipisicing sed non",
         "age" : 28,
         "married" : false,
         "subentries" : [ 
           { 
             "comment" : "ea magna amet, amet, est minim minim incididunt voluptate proident, aute anim non enim reprehenderit consequat. sint laboris Excepteur tempor voluptate culpa reprehenderit Excepteur fugiat esse sunt eu estsint dolore consequat. in nulla nulla cupidatatin dolore in dolore sit ea id velit non in consequat. eu exercitation dolor estUt velit ex"
             }
           ]
         },
       { 
         "id" : 2,
         "name" : "Frank Williams",
         "eg_date_time" : "Sun Feb 06 1977 23:58:34 GMT-0800 (PST)",
         "eg_date_assembled" : "20:36:00",
         "title" : "dolore amet quis ",
         "description" : "Ut in nisi nulla Ut in in ex et cillum Lorem mollit cillum Duis",
         "age" : 19,
         "married" : false,
         "subentries" : [ 
           { 
             "comment" : "tempor enim reprehenderit in Lorem dolore officia in reprehenderit do nostrud velit Lorem nulla esse enimvelit cillum non laborumid occaecat occaecat est non pariatur. nulla ad et occaecat tempor deserunt officia aliqua. sunt veniam, Lorem dolore irure doloreproident, in ut nostrud et culpa do Duis Duis laboris deserunt reprehenderit do ut velit deserunt magna tempor sint minim magna enim dolor Excepteur sit esse dolore in do laborum eiusmod"
             }
           ]
         }
       ]
     }

