var mock = require( './mock-json.js' );

function template( owner_id, type ) {
	
	var mock_entry_tmpl = {
	
		"id|+1" 			: Math.floor( Math.random() * 1000 ),
		"owner_id" 			: owner_id,
		"type" 				: type,
		"msgid|20-20" 		: "@LETTER_LOWER",
		"sender" 			: "@EMAIL",
		"recip"  			: "@EMAIL",
		"recip_name"		: "@FIRST_NAME @LAST_NAME",
		"subject|5-20"		: "@LOREM ",
		"sent_on"			: "@DATE_TIME",
		"resent_on|0-5"		: [
			"@DATE_TIME"
		],
		"opened_on|0-15"	: [
			"@DATE_TIME"
		],
		"user_agent"		: "@LOREM-@NUMBER",
		"url"				: "http://@LOREM.com/@LOREM"
	};
	
	if( type === 'mls' ) {
		
		var ext_data = {
			"mls_number"	: "@LOREM-@NUMBER",
			"price|5-7"			: "@NUMBER",
			"mls_system"	: "@LOREM"
		}
		
		mock_entry_tmpl.ext_data = ext_data;
	}
	
	var mock_list_tmpl = {
	
		"entries|1-1000" : [
		
			mock_entry_tmpl
		]
	};

	return mock_list_tmpl;
}


var r = mock.generate( template( 100, 'mls' ) );

console.log( mock.formatJSON( r, '  ' ) );