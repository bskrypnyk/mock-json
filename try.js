var mock = require( './mock-json.js' );

var tmpl1 = {
	
	"entries|1-10" : [
		{
			"id|+1"					: 0,
			"funky"              : function() { return "I am a function!"; },
			"name"					: "@FIRST_NAME @LAST_NAME",
			"eg_date_time"			: "@DATE_TIME",
			"eg_date_assembled"		: "@TIME_HH:@TIME_MM:@TIME_SS",
			"title|2-4"				: "@LOREM ",
            "description"			: "@LOREM_IPSUM",
            "age|18-31"				: 18, 
            "married|0-1"			: true,
            "subentries|0-3"		: [
            	{
	            	"comment|1-10"		: "@LOREM_IPSUM",
            	}
            ]
		}
	]
};

var r = mock.generate( tmpl1 );

console.log( mock.formatJSON( r, "   " ) );
